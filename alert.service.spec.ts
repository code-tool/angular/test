import { TestBed } from "@angular/core/testing";
import { AlertService } from "./alert.service";
import { MAT_DIALOG_SCROLL_STRATEGY, MatDialog, MatDialogModule } from "@angular/material/dialog";
import { of } from "rxjs";
import { DialogConfirmComponent } from "../components/alerts/dialog/dialog-confirm/dialog-confirm.component";
import { DialogInformacionComponent } from "../components/alerts/dialog/dialog-informacion/dialog-informacion.component";
import { commonConstants } from "../core/others/common-constants";
import { DialogConfirmLocalComponent } from "../components/alerts/dialog/dialog-confirm-local/dialog-confirm-local.component";

describe('AlertService', () => {
    let service: AlertService;
    let dialog: MatDialog;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            providers: [AlertService
            ]
        });
        service = TestBed.inject(AlertService);
        dialog = TestBed.inject(MatDialog);

    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });


    it('debería abrir un cuadro de diálogo de confirmación', () => {
        // Espía en el método 'open' del servicio de diálogo
        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(true), // Simula que el diálogo fue cerrado con 'true'
        } as any);
        const promise = service.confirmDialog('¿Estás seguro?');
        expect(dialogSpy).toHaveBeenCalledWith(DialogConfirmComponent, {
            disableClose: true,
            data: { message: '¿Estás seguro?' },
        });
        promise.then((result) => {
            expect(result).toBe(true);
        });
    });

    it('debería abrir un cuadro de diálogo de confirmación y retornar false', () => {
        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(false), // Simula que el diálogo fue cerrado con 'true'
        } as any);
        const promise = service.confirmDialog('¿Estás seguro?');
        expect(dialogSpy).toHaveBeenCalledWith(DialogConfirmComponent, {
            disableClose: true,
            data: { message: '¿Estás seguro?' },
        });
        promise.then((result) => {
            expect(result).toBe(false);
        });
    });

    it('debería abrir un cuadro de diálogo de información', () => {
        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(true), // Simula que el diálogo fue cerrado
        } as any);
        service.openSuccessDialog('Éxito', 'Operación completada', 'Cerrar', (result: any) => {
            expect(dialogSpy).toHaveBeenCalledWith(DialogInformacionComponent, {
                disableClose: true,
                width: '300px',
                data: jasmine.objectContaining({
                    title: 'Éxito',
                    content: 'Operación completada',
                    closeButtonLabel: 'Cerrar',
                }),
            });
            expect(result).toBe(true);
        });
    });

    it('debería abrir un cuadro de diálogo de alert generico', () => {
        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(true), // Simula que el diálogo fue cerrado
        } as any);
        service.alertGenerico('Éxito', 'Operación completada', 'Cerrar', (result: any) => {
            expect(dialogSpy).toHaveBeenCalledWith(DialogInformacionComponent, {
                disableClose: true,
                width: '300px',
                data: jasmine.objectContaining({
                    title: 'Éxito',
                    content: 'Operación completada',
                    closeButtonLabel: 'Cerrar',
                }),
            });
            expect(result).toBe(true);
        });
    });

    it('debería abrir un cuadro de diálogo de alert generico y entra al if de with', () => {
        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(true), // Simula que el diálogo fue cerrado
        } as any);
        service.alertGenerico('Éxito', 'Operación completada', 'Cerrar', (result: any) => {
            expect(dialogSpy).toHaveBeenCalledWith(DialogInformacionComponent, {
                disableClose: true,
                width: '100px',
                data: jasmine.objectContaining({
                    title: 'Éxito',
                    content: 'Operación completada',
                    closeButtonLabel: 'Cerrar',
                }),
            });
            expect(result).toBe(true);
        }, 100);
    });


    it('Deberia ejecutar builDataMessage correctamente', async () => {
        const codigo = 'codigo';
        const tipo = 1;
        localStorage.setItem(codigo, '{ "msgone":"2323" }');
        localStorage.setItem(commonConstants.tiposAcciones.idExitoso, '{ "title":"anth" }');
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });


    it('Deberia ejecutar builDataMessage else ', async () => {
        const codigo = 'codigo';
        const tipo = 1;
        localStorage.setItem(codigo, '{}');
        localStorage.setItem(commonConstants.tiposAcciones.idExitoso, '{}');
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });


    it('Deberia ejecutar builDataMessage else other', async () => {
        const codigo = 'codigo';
        const tipo = 1;
        localStorage.removeItem(codigo);
        localStorage.removeItem(commonConstants.tiposAcciones.idExitoso);
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });


    it('Deberia ejecutar builDataMessage correctamente tipo 2', async () => {
        const codigo = 'codigo';
        const tipo = 1;
        localStorage.setItem(codigo, '{ "msgone":"2323" }');
        localStorage.setItem(commonConstants.tiposAcciones.idValidacion, '{ "title":"anth" }');
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });



    it('Deberia ejecutar builDataMessage else tipo 2', async () => {
        const codigo = 'codigo';
        const tipo = 2;
        localStorage.setItem(codigo, '{}');
        localStorage.setItem(commonConstants.tiposAcciones.idValidacion, '{}');
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });

    it('Deberia ejecutar builDataMessage else other tipo 2', async () => {
        const codigo = 'codigo';
        const tipo = 2;
        localStorage.removeItem(codigo);
        localStorage.removeItem(commonConstants.tiposAcciones.idValidacion);
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });

    it('Deberia ejecutar builDataMessage correctamente tipo 3', async () => {
        const codigo = 'codigo';
        const tipo = 3;
        localStorage.setItem(codigo, '{ "msgone":"2323" }');
        localStorage.setItem(commonConstants.tiposAcciones.idError, '{ "title":"anth" }');
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });


    it('Deberia ejecutar builDataMessage else tipo 3', async () => {
        const codigo = 'codigo';
        const tipo = 3;
        localStorage.setItem(codigo, '{}');
        localStorage.setItem(commonConstants.tiposAcciones.idError, '{}');
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });




    it('Deberia ejecutar builDataMessage else other tipo 3', async () => {
        const codigo = 'codigo';
        const tipo = 3;
        localStorage.removeItem(codigo);
        localStorage.removeItem(commonConstants.tiposAcciones.idError);
        const response = await service.builDataMessage(codigo, tipo);
        expect(response).toBeDefined();
    });


    it('debería abrir un cuadro de diálogo de confirmación return data LocalStorage null', () => {
        const button='codigo';
        localStorage.removeItem(button);
        localStorage.removeItem(commonConstants.tiposAcciones.idConfimar);

        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(true), // Simula que el diálogo fue cerrado
        }as any);
        service.confirmCM('tu-button').then((result: boolean) => {
            expect(dialogSpy).toHaveBeenCalledWith(DialogConfirmLocalComponent, {
                disableClose: true,
                width: '300px',
                data: jasmine.objectContaining({
                    msgone: '¿Está seguro de realizar la acción?',
                    codigo: 'tu-button',
                    title: 'Confirmar Acción',
                    buttonYes: 'Aceptar',
                    buttonNou: 'Denegar',
                }),
            });
            expect(result).toBe(true);
        });
    });


    it('debería abrir un cuadro de diálogo de confirmación return data LocalStorage exist', () => {
        const button='codigo';
        localStorage.setItem(button, '{"codigo":"code","msgone":"mensaje uno"}');
        localStorage.setItem(commonConstants.tiposAcciones.idConfimar, '{"title":"confirm","btnone":"botontuno","btntwo":"NO" }');

        const dialogSpy = spyOn(dialog, 'open').and.returnValue({
            afterClosed: () => of(false), // Simula que el diálogo fue cerrado
        }as any);
        service.confirmCM(button).then((result: boolean) => {
            expect(dialogSpy).toHaveBeenCalledWith(DialogConfirmLocalComponent, {
                disableClose: true,
                width: '300px',
                data: {
                    codigo: 'codigo',
                    msgone: 'mensaje uno',
                    title: 'confirm',
                    buttonYes: 'botontuno',
                    buttonNou: 'NO'
                }
            });
            expect(result).toBe(false);
        });
    });
















});
