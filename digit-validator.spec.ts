import {DigitValidator} from "./digit.validator";

describe('DigitValidator', () => {
  let digitValidator: DigitValidator;

  beforeEach(() => {
    digitValidator = new DigitValidator();
  });

  it('should create an instance', () => {
    expect(new DigitValidator()).toBeTruthy();
  });


  // Para validar sólo números
  it('debería permitir solo números enteros positivos', () => {
    const result = digitValidator.validateOnlyNumber('123');
    expect(result).toBeTruthy();
  });

  it('no debería permitir caracteres no numéricos', () => {
    const result = digitValidator.validateOnlyNumber('abc');
    expect(result).toBeFalsy();
  });


  // Para validar decimales
  it('debería permitir solo números decimales y el punto', () => {
    const result1 = digitValidator.validateDecimal({ keyCode: 48 }); // 0
    expect(result1).toBeTruthy();

    const result2 = digitValidator.validateDecimal({ keyCode: 53 }); // 5
    expect(result2).toBeTruthy();

    const result3 = digitValidator.validateDecimal({ keyCode: 46 }); // Punto
    expect(result3).toBeTruthy();

    const result4 = digitValidator.validateDecimal({ keyCode: 65 }); // Letra (no permitido)
    expect(result4).toBeFalsy();
  });



  // Para validar letras y caracteres especiales
  it('debería permitir solo números enteros positivos', () => {
    const result = digitValidator.validateOnlyLetterAndSpecial('abc');
    expect(result).toBeTruthy();
  });

  it('no debería permitir caracteres no numéricos', () => {
    const result = digitValidator.validateOnlyLetterAndSpecial('123');
    expect(result).toBeFalsy();
  });

});
