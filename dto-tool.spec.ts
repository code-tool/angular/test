import {DTOTool} from './dto-tool';
import {HttpParams} from "@angular/common/http";

describe('DTOTool', () => {
  it('should create an instance', () => {
    expect(new DTOTool()).toBeTruthy();
  });



  // Retornar cadenas
  it('Retornar cadenas 1',()=>{
    const result=DTOTool.returnStringFromValue('Abc');
    expect(result).toEqual('Abc');
  });

  it('Retornar cadenas 2',()=>{
    const result=DTOTool.returnStringFromValue('');
    expect(result).toEqual('');
  });



  // Para saber si setea a parametros
  it('Para saber si setea a parametros 1',()=>{
    const params: HttpParams=new HttpParams();
    const result=DTOTool.setParam('Abc', params, 'paramName');
    expect(result.get('paramName')).toEqual('Abc');
  });

  it('Para saber si setea a parametros 2',()=>{
    const params: HttpParams=new HttpParams()
    const result=DTOTool.setParam('', params, 'paramName');
    expect(result).toEqual(params);
  });

});
