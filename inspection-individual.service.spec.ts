import { TestBed } from '@angular/core/testing';

import { DatePipe } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { Constants } from '../classes/constants';
import { Page } from '../dtos/Page';
import { GenerateAplicationPostDTO } from '../dtos/generate-aplication-post-dto';
import { InspectionFiltersDTO } from '../dtos/inspection-filters-dto';
import { InspectionIndividualActaDTO } from '../dtos/inspection-individual-acta-dto';
import { InspectionIndividualGetDTO } from '../dtos/inspection-individual-get-dto';
import { InspectionIndividualPostDTO } from '../dtos/inspection-individual-post-dto';
import { InspectionIndividualTrayDTO } from '../dtos/inspection-individual-tray-dto';
import { DateService } from './date.service';
import { InspectionIndividualService } from './inspection-individual.service';

describe('InspectionIndividualService', () => {
  let inspectionIndividualService: InspectionIndividualService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let dateServiceSpy: jasmine.SpyObj<DateService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InspectionIndividualService, DatePipe]
    });
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post', 'get', 'put']);
    dateServiceSpy = jasmine.createSpyObj('DateService', ['formatStartDate', 'formatEndDate', 'formatToSaveDate']);
    inspectionIndividualService = TestBed.inject(InspectionIndividualService);
    inspectionIndividualService = new InspectionIndividualService(httpClientSpy as any, dateServiceSpy as any);
  });

  // created
  it('should be created', () => {
    expect(inspectionIndividualService).toBeTruthy();
  });


  // saveInspect
  it('saveInspect', (done: DoneFn) => {
    const mockPost: InspectionIndividualPostDTO[] = [];

    httpClientSpy.post.and.returnValue(of({}));

    inspectionIndividualService.saveInspection(mockPost).subscribe(
      response => {
        expect(response).toEqual({});
        done();
      }
    );
  });

  // find
  it('find', (done: DoneFn) => {
    const mockResponse: Page<InspectionIndividualTrayDTO> = {
      content: [],
      totalElements: 0,
      size: 0,
      number: 0 // number
    };
    const inspectionFiltersDTO: InspectionFiltersDTO = {
      type:Constants.PATH_INDIVIDUAL_INSPECTION,
      evaluationTypeId: 'string', // evaluationTypeId
      applicationCode: 'string', // applicationCode
      userZEE: 'string', // userZEE
      regimeTypeId: 'string', // regimeTypeId
      operationTypeId: 'string', // operationTypeId
      draftCode: 'string', // draftCode
      operationCode: 'string', // operationCode
      outputCode: 'string', // outputCode
      driver: 'string', // driver
      plateNumber: 'string', // plateNumber
      assignmentTypeId: 'string', // assignmentTypeId
      stateId: 'string', // stateId
      resultId: 'string', // resultId
      levelId: 'string', // levelId
      aforador: 'string', // aforador
      startDate: new Date(), // startDate
      endDate: new Date(), // endDate
      page: 10, // page
      size: 10,// size
      loggedUserTypeId: 'string',// loggedUserTypeId
      loggedRoleId: 'string', // loggedRoleId
      loggedUserId: 'string', // loggedUserId
      extension: 'string' // extension
    };
    const mockString: string = '2023-01-01 00:00:00';


    httpClientSpy.get.and.returnValue(of(mockResponse));
    dateServiceSpy.formatStartDate.and.returnValue(mockString);
    dateServiceSpy.formatEndDate.and.returnValue(mockString);

    inspectionIndividualService.find(inspectionFiltersDTO).subscribe(
      response => {
        expect(response).toEqual(mockResponse);
        done();
      }
    );
  });


  // find with some params null
  it('find', (done: DoneFn) => {
    const mockResponse: Page<InspectionIndividualTrayDTO> = {
      content: [],
      totalElements: 0,
      size: 0,
      number: 0 // number
    };
    const inspectionFiltersDTO: InspectionFiltersDTO = {
      type:Constants.PATH_INDIVIDUAL_INSPECTION,
      evaluationTypeId: null, // evaluationTypeId
      applicationCode: null, // applicationCode
      userZEE: null, // userZEE
      regimeTypeId: null, // regimeTypeId
      operationTypeId: null, // operationTypeId
      draftCode: null, // draftCode
      operationCode: null, // operationCode
      outputCode: 'string', // outputCode
      driver: null, // driver
      plateNumber: null, // plateNumber
      assignmentTypeId: null, // assignmentTypeId
      stateId: null, // stateId
      resultId: 'string', // resultId
      levelId: null, // levelId
      aforador: null, // aforador
      startDate: new Date(), // startDate
      endDate: new Date(), // endDate
      page: null, // page
      size: null,// size
      loggedUserTypeId: 'string',// loggedUserTypeId
      loggedRoleId: 'string', // loggedRoleId
      loggedUserId: 'string', // loggedUserId
      extension: null // extension
    };
    const mockString: string = '2023-01-01 00:00:00';


    httpClientSpy.get.and.returnValue(of(mockResponse));
    dateServiceSpy.formatStartDate.and.returnValue(mockString);
    dateServiceSpy.formatEndDate.and.returnValue(mockString);

    inspectionIndividualService.find(inspectionFiltersDTO).subscribe(
      response => {
        expect(response).toEqual(mockResponse);
        done();
      }
    );
  });


  // export
  it('export', (done: DoneFn) => {
    const inspectionFiltersDTO: InspectionFiltersDTO = {
      type:Constants.PATH_INDIVIDUAL_INSPECTION,
      evaluationTypeId: 'string', // evaluationTypeId
      applicationCode: 'string', // applicationCode
      userZEE: 'string', // userZEE
      regimeTypeId: 'string', // regimeTypeId
      operationTypeId: 'string', // operationTypeId
      draftCode: 'string', // draftCode
      operationCode: 'string', // operationCode
      outputCode: 'string', // outputCode
      driver: 'string', // driver
      plateNumber: 'string', // plateNumber
      assignmentTypeId: 'string', // assignmentTypeId
      stateId: 'string', // stateId
      resultId: 'string', // resultId
      levelId: 'string', // levelId
      aforador: 'string', // aforador
      startDate: new Date(), // startDate
      endDate: new Date(), // endDate
      page: 10, // page
      size: 10,// size
      loggedUserTypeId: 'string',// loggedUserTypeId
      loggedRoleId: 'string', // loggedRoleId
      loggedUserId: 'string', // loggedUserId
      extension: 'string' // extension
    };
    const mockString: string = '2023-01-01 00:00:00';
    httpClientSpy.get.and.returnValue(of(new Blob()));
    dateServiceSpy.formatStartDate.and.returnValue(mockString);
    dateServiceSpy.formatEndDate.and.returnValue(mockString);
    dateServiceSpy.formatToSaveDate.and.returnValue(mockString);

    inspectionIndividualService.export(inspectionFiltersDTO).subscribe(
      response => {
        expect(response).toEqual();
        done();
      }
    );
  });


  // findById
  it('findById', (done: DoneFn) => {
    const mockId: string = 'id';
    const mockResponse: InspectionIndividualGetDTO = { id: 'id' };

    httpClientSpy.get.and.returnValue(of(mockResponse));

    inspectionIndividualService.findById(mockId).subscribe(
      response => {
        expect(response).toEqual(mockResponse);
        done();
      }
    );
  });


  // generateApplication
  it('generateApplication', (done: DoneFn) => {
    const mockPost: GenerateAplicationPostDTO = {
      id: 'id',
      motiveId: 'string',
      comment: 'string',
      loggedUserId: 'id'
    };
    const mockResponse: string = 'string';

    httpClientSpy.put.and.returnValue(of(mockResponse));

    inspectionIndividualService.generateApplication(mockPost).subscribe(
      response => {
        expect(response).toEqual(mockResponse);
        done();
      }
    );
  });

  // noInspect
  it('noInspect', (done: DoneFn) => {
    const mockPost: string = 'id'
    const mockResponse: string = 'id';

    httpClientSpy.put.and.returnValue(of(mockResponse));

    inspectionIndividualService.noInspect(mockPost).subscribe(
      response => {
        expect(response).toEqual(mockResponse);
        done();
      }
    );
  });


  // findAllActasByInspeccionId
  it('findAllActasByInspeccionId', (done: DoneFn) => {
    const mockPost: string = 'id'
    const mockResponse: InspectionIndividualActaDTO[] = [];

    httpClientSpy.get.and.returnValue(of(mockResponse));

    inspectionIndividualService.findAllActasByInspeccionId(mockPost).subscribe(
      response => {
        expect(response).toEqual(mockResponse);
        done();
      }
    );
  });


  // builtParameters
  it('builtParameters', (done: DoneFn) => {
    const inspectionFiltersDTO: InspectionFiltersDTO = {
      type:Constants.PATH_INDIVIDUAL_INSPECTION,
      evaluationTypeId: 'string', // evaluationTypeId
      applicationCode: 'string', // applicationCode
      userZEE: 'string', // userZEE
      regimeTypeId: 'string', // regimeTypeId
      operationTypeId: 'string', // operationTypeId
      draftCode: 'string', // draftCode
      operationCode: 'string', // operationCode
      outputCode: 'string', // outputCode
      driver: 'string', // driver
      plateNumber: 'string', // plateNumber
      assignmentTypeId: 'string', // assignmentTypeId
      stateId: 'string', // stateId
      resultId: 'string', // resultId
      levelId: 'string', // levelId
      aforador: 'string', // aforador
      startDate: new Date(), // startDate
      endDate: new Date(), // endDate
      page: 10, // page
      size: 10,// size
      loggedUserTypeId: Constants.LOGGED_USER_TYPE_ID,// loggedUserTypeId
      loggedRoleId: Constants.LOGGED_ROLE_ID, // loggedRoleId
      loggedUserId: 'string', // loggedUserId
      extension: 'xlsx' // extension
    };


    let params: HttpParams = new HttpParams();
    params = params.append('evaluationTypeId', 'string');
    params = params.append('applicationCode', 'string');
    params = params.append('userZEE', 'string');
    params = params.append('regimeTypeId', Constants.REGIME_ZEE_ID);
    params = params.append('operationTypeId', 'string');
    params = params.append('draftCode', 'string');
    params = params.append('operationCode', 'string');
    params = params.append('driver', 'string');
    params = params.append('plateNumber', 'string');
    params = params.append('assignmentTypeId', 'string');
    params = params.append('stateId', 'string');
    params = params.append('levelId', 'string');
    params = params.append('aforador', 'string');
    params = params.append('startDate', '2023-01-01 00:00:00');
    params = params.append('endDate', '2023-01-01 23:59:59');
    params = params.append('page', 10);
    params = params.append('size', 10);
    params = params.append('loggedUserTypeId', Constants.LOGGED_USER_TYPE_ID);
    params = params.append('loggedRoleId', Constants.LOGGED_ROLE_ID);
    params = params.append('loggedUserId', Constants.LOGGED_USER_ID);
    params = params.append('extension', 'xlsx');

    dateServiceSpy.formatStartDate.and.returnValue('2023-01-01 00:00:00');
    dateServiceSpy.formatEndDate.and.returnValue('2023-01-01 23:59:59');

    let response: HttpParams = inspectionIndividualService.builtParameters(inspectionFiltersDTO);
    expect(response.get('evaluationTypeId')).toEqual('string');
    expect(response.get('applicationCode')).toEqual('string');
    done();
  });

});
