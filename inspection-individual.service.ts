import { HttpClient, HttpParams } from '@angular/common/http'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { Injectable } from '@angular/core'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { Observable } from 'rxjs'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { map } from 'rxjs/operators'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { environment } from 'src/environments/environment'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { Constants } from '../classes/constants'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { Page } from '../dtos/Page'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { GenerateAplicationPostDTO } from '../dtos/generate-aplication-post-dto'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { InspectionFiltersDTO } from '../dtos/inspection-filters-dto'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { InspectionIndividualActaDTO } from '../dtos/inspection-individual-acta-dto'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { InspectionIndividualGetDTO } from '../dtos/inspection-individual-get-dto'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { InspectionIndividualPostDTO } from '../dtos/inspection-individual-post-dto'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { InspectionIndividualTrayDTO } from '../dtos/inspection-individual-tray-dto'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { DateService } from './date.service'; // Importamos las librerías, clase, interface, etc. necesarias para este componente
import { DTOTool } from '../classes/dto-tool';

@Injectable({
  providedIn: 'root'
})
export class InspectionIndividualService {

  private SERVER_INDIVIDUAL = `${environment.API_AFORO}/inspeccion-individual`; // Endpoint SERVER_INDIVIDUAL

  // constructor
  constructor(
    private httpClient: HttpClient, // httpClient
    private dateService: DateService // dateService
  ) {
  }

  // Guardar inspeción
  saveInspection(inspectionDTO: InspectionIndividualPostDTO[]): Observable<InspectionIndividualActaDTO> {
    return this.httpClient.post<InspectionIndividualActaDTO>(this.SERVER_INDIVIDUAL, inspectionDTO); // DEvolvemos el observable de respuesta
  }

  // find
  find(filters: InspectionFiltersDTO): Observable<Page<InspectionIndividualTrayDTO>> {
    let params: HttpParams = this.builtParameters(filters); // Construimos los parámetros
    return this.httpClient.get<Page<InspectionIndividualTrayDTO>>(`${this.SERVER_INDIVIDUAL}`, { params }); // Devolvemos el observable de respuesta
  }

  // Method to export
  export(filters: InspectionFiltersDTO): Observable<void> {
    let params: HttpParams = this.builtParameters(filters); // Construimos la variable de los parametros
    return this.httpClient
      .get(`${this.SERVER_INDIVIDUAL}/exportar`, { params, responseType: 'blob' })
      .pipe(
        map((data) => {
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(new Blob([data]));
          link.setAttribute('download', `Solicitudes_inspección_Descarga_Levante_mercancía_${this.dateService.formatToSaveDate(new Date())}_.${filters.extension}`);
          document.body.appendChild(link);
          link.click();
        })
      );
  }

  // Método findById
  findById(id: string): Observable<InspectionIndividualGetDTO> {
    return this.httpClient.get<InspectionIndividualGetDTO>(`${this.SERVER_INDIVIDUAL}/${id}`); // Devolvemos observable de respuesta
  }

  // Método generateApplication
  generateApplication(formValue: GenerateAplicationPostDTO): Observable<string> {
    let params: HttpParams = new HttpParams() // Inicializamos varible de HttpParams
      .set('id', formValue.id) // Set param id
      .set('motiveId', formValue.motiveId) // Set param motiveId
      .set('comment', formValue.comment) // Set param comment
      .set('loggedUserId', formValue.loggedUserId); // Set param loggedUserId
    return this.httpClient.put<string>(`${this.SERVER_INDIVIDUAL}/generar-solicitud?${params}`, {}); // Devolvemos observable de respuesta
  }

  // Método noInspect
  noInspect(id: string): Observable<string> {
    let params: HttpParams = new HttpParams() // Inicializamos variable de parametros
      .set('id', id); // Set param id
    return this.httpClient.put<string>(`${this.SERVER_INDIVIDUAL}/no_inspeccionar?${params}`, {}); // Devolvemos observable de respuesta
  }

  // Método findAllActasByInspeccionId
  findAllActasByInspeccionId(id: string): Observable<InspectionIndividualActaDTO[]> {
    return this.httpClient.get<InspectionIndividualActaDTO[]>(`${this.SERVER_INDIVIDUAL}/actas/${id}`); // Devolvemos observable de respuesta 
  }

  // Built parameters (builtParameters)
  public builtParameters(filters: InspectionFiltersDTO): HttpParams {
    let params: HttpParams = new HttpParams(); // Inicializamos la variable de los parámetros
    params = params.set('type', Constants.PATH_INDIVIDUAL_INSPECTION); // Parámetro de Tipo de Inspección
    params = DTOTool.setParam(filters.evaluationTypeId, params, 'evaluationTypeId'); // Set param evaluationTypeId
    params = DTOTool.setParam(filters.applicationCode, params, 'applicationCode'); // Set param applicationCode
    params = DTOTool.setParam(filters.userZEE, params, 'userZEE'); // Set param userZEE
    params = params.set('regimeTypeId', Constants.REGIME_ZEE_ID); // Set param regimeTypeId
    params = DTOTool.setParam(filters.operationTypeId, params, 'operationTypeId'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.draftCode, params, 'draftCode'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.operationCode, params, 'operationCode'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.driver, params, 'driver'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.plateNumber, params, 'plateNumber'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.assignmentTypeId, params, 'assignmentTypeId'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.stateId, params, 'stateId'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.levelId, params, 'levelId'); // Se agrega el parámetro
    params = DTOTool.setParam(filters.aforador, params, 'aforador'); // Se agrega el parámetro
    params = params.set('startDate', this.dateService.formatStartDate(filters.startDate as Date)); // Set param startDate
    params = params.set('endDate', this.dateService.formatEndDate(filters.endDate as Date)); // Set param endDate
    if (filters.page !== null && filters.page > -1) params = params.set('page', filters.page); // Set param page
    if (filters.size !== null && filters.size > -1) params = params.set('size', filters.size); // Set param size
    params = params.set('loggedUserTypeId', Constants.LOGGED_USER_TYPE_ID); // Set param loggedUserTypeId
    params = params.set('loggedRoleId', Constants.LOGGED_ROLE_ID); // Set param loggedRoleId
    params = params.set('loggedUserId', Constants.LOGGED_USER_ID); // Set param loggedUserId
    params = DTOTool.setParam(filters.extension, params, 'extension'); // Se agrega el parámetro
    return params; // Devolvemos variable de los parámetros
  }

}
