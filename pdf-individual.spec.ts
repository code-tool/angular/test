import { PdfIndividual } from './pdf-individual';
import pdfMake from 'pdfmake/build/pdfmake';
import {Constants} from "../constants";
import {InspectionIndividualActaDTO} from "../../dtos/inspection-individual-acta-dto";

describe('PdfIndividual', () => {
  let pdfMakeCreatePdfSpy: jasmine.Spy;

  beforeEach(() => {
    pdfMakeCreatePdfSpy = spyOn(pdfMake, 'createPdf').and.returnValue({
      download: jasmine.createSpy('download'),
      open: jasmine.createSpy('open')
    });
  });


  it('should create an instance', () => {
    expect(new PdfIndividual()).toBeTruthy();
  });


  it('debería llamar a createPdf con la definición del PDF y descargarlo', () => {
    const inspectionActaDTO:InspectionIndividualActaDTO = {
      id: '1', //id
      zone: 'ZEE', //zone
      economicZoneLogo: 'string',
      tipoEvaluacionDescripcion: 'Abc', //tipoEvaluacionDescripcion
      withActa: true, // Campo que guadar si la inspección tiene acta generada
      numeroActa: 'ABC-123', //numeroActa
      fechaInspeccion: '12/12/2023', //fechaInspeccion
      actaResultMissing: true, // Faltante
      actaResultSurplus: true, // Sobrante
      actaResultCompliant: true, // Conforme
      dayInspection: '12', //dayInspection
      monthDescriptionInspection: 'Diciembre', //monthDescriptionInspection
      yearInspection: '2024', //yearInspection
      almacenCode: 'ABC', //almacenCode
      timeInspection: '12:00:00', //timeInspection
      userFullName: 'Nombre Completos', // Razón Social del Usuario ZEE
      usuarioZeeDocumentTypeDescripcion: 'Abc', // Tipo de documento del Usuario ZEE
      usuarioZeeNumeroDocumento: '12345678', // Número de documento del Usuario ZEE
      signatoryFullName: 'Nombre Completo', // Nombre completo del representante del usuario quien firma
      signatoryDocument: '12345678', // Documento del representante del usuario quien firma
      aforadorFullName: 'Nombre Completo', // Nombre completo del aforador
      aforadorWorkerZeeDocumentTypeDescripcion: 'DNI', // Tipo de documento del aforador
      aforadorWorkerZeeNumeroDocumento: '12345678', // Número de documento del aforador
      draft: 'ABC-123', // Código DRAFT
      anuncioLlegadaDocumentoNumero: 'ABC-123', //Número de documento
      merchandiseReceptionId: '1', //merchandiseReceptionId
      evaluationDescripcion: 'Abc', //evaluationDescripcion
      othersDescripcion: 'Abc', //othersDescripcion
      resultadoInspeccion: 'Todo bien', //resultadoInspeccion
      firmaUsuario: 'string', // Firma del usuario
      firmaAforador: 'string', // Firma del aforador
      firmaTransportista: 'string', //Firma de transportista
      //
      inspectionStateId: '1', // Id del estado de la inspeción
      inspeccionTipoEvaluacionId: '1', // Id del tipo de evaluación de la inspección
      inspeccionTipoEvaluacionDescripcion: 'Abc', // Descripción del tipo de evaluación de la inspección
      inspeccionCodigoOperacion: 'ABC-123', // Código de operación
      inspeccionAnuncioLlegadaCodigo: 'ABC-123', // Código DRAFT
      inspeccionAnuncioLlegadaDocumentoNumero: 'ABC-123', // Documento aduanero
      inspeccionTransportistaEmpresaTransporteRazonSocial: 'string', // Razón social de la empresa de transporte
      inspeccionTransportistaEmpresaTransporteNumeroDocumento: 'string', // Número de documento de la empresa de transporte
      inspeccionTransportistaConductorFullName: 'string', // Nombre completo del conductor
      inspeccionTransportistaConductorDocumentTypeDescripcion: 'string', // Tipo de documento del conductor
      inspeccionTransportistaConductorDocumentNumber: 'string', // Número de documento completo del conductor
      anuncioLlegadaTipoMercanciaId: 'string', // Id del tipo de mercancía
      cpct: 'string', // Carta porte
      placaContenedor: 'string', // placa-contenedor
      depositoDestinoCode: ['string'], // Almacenes
      precintoNumber: 'string', // Número de precinto
      levateDescripcion: 'string' // Descripción del levante
    };

    PdfIndividual.createPDF(inspectionActaDTO, Constants.PDF_MODE_DOWNLOAD);

    expect(pdfMake.createPdf).toHaveBeenCalledOnceWith(jasmine.any(Object));
    expect(pdfMakeCreatePdfSpy().download).toHaveBeenCalledOnceWith(jasmine.any(String));
    expect(pdfMakeCreatePdfSpy().open).not.toHaveBeenCalled();
  });


  it('debería llamar a createPdf con la definición del PDF y abrirlo', () => {
    const inspectionActaDTO:InspectionIndividualActaDTO = {
      id: '1', //id
      zone: 'ZEE', //zone
      economicZoneLogo: 'string',
      tipoEvaluacionDescripcion: 'Abc', //tipoEvaluacionDescripcion
      withActa: true, // Campo que guadar si la inspección tiene acta generada
      numeroActa: 'ABC-123', //numeroActa
      fechaInspeccion: '12/12/2023', //fechaInspeccion
      actaResultMissing: true, // Faltante
      actaResultSurplus: true, // Sobrante
      actaResultCompliant: true, // Conforme
      dayInspection: '12', //dayInspection
      monthDescriptionInspection: 'Diciembre', //monthDescriptionInspection
      yearInspection: '2024', //yearInspection
      almacenCode: 'ABC', //almacenCode
      timeInspection: '12:00:00', //timeInspection
      userFullName: 'Nombre Completos', // Razón Social del Usuario ZEE
      usuarioZeeDocumentTypeDescripcion: 'Abc', // Tipo de documento del Usuario ZEE
      usuarioZeeNumeroDocumento: '12345678', // Número de documento del Usuario ZEE
      signatoryFullName: 'Nombre Completo', // Nombre completo del representante del usuario quien firma
      signatoryDocument: '12345678', // Documento del representante del usuario quien firma
      aforadorFullName: 'Nombre Completo', // Nombre completo del aforador
      aforadorWorkerZeeDocumentTypeDescripcion: 'DNI', // Tipo de documento del aforador
      aforadorWorkerZeeNumeroDocumento: '12345678', // Número de documento del aforador
      draft: 'ABC-123', // Código DRAFT
      anuncioLlegadaDocumentoNumero: 'ABC-123', //Número de documento
      merchandiseReceptionId: '1', //merchandiseReceptionId
      evaluationDescripcion: 'Abc', //evaluationDescripcion
      othersDescripcion: 'Abc', //othersDescripcion
      resultadoInspeccion: 'Todo bien', //resultadoInspeccion
      firmaUsuario: 'string', // Firma del usuario
      firmaAforador: 'string', // Firma del aforador
      firmaTransportista: 'string', //Firma de transportista
      //
      inspectionStateId: '1', // Id del estado de la inspeción
      inspeccionTipoEvaluacionId: '1', // Id del tipo de evaluación de la inspección
      inspeccionTipoEvaluacionDescripcion: 'Abc', // Descripción del tipo de evaluación de la inspección
      inspeccionCodigoOperacion: 'ABC-123', // Código de operación
      inspeccionAnuncioLlegadaCodigo: 'ABC-123', // Código DRAFT
      inspeccionAnuncioLlegadaDocumentoNumero: 'ABC-123', // Documento aduanero
      inspeccionTransportistaEmpresaTransporteRazonSocial: 'string', // Razón social de la empresa de transporte
      inspeccionTransportistaEmpresaTransporteNumeroDocumento: 'string', // Número de documento de la empresa de transporte
      inspeccionTransportistaConductorFullName: 'string', // Nombre completo del conductor
      inspeccionTransportistaConductorDocumentTypeDescripcion: 'string', // Tipo de documento del conductor
      inspeccionTransportistaConductorDocumentNumber: 'string', // Número de documento completo del conductor
      anuncioLlegadaTipoMercanciaId: 'string', // Id del tipo de mercancía
      cpct: 'string', // Carta porte
      placaContenedor: 'string', // placa-contenedor
      depositoDestinoCode: ['string'], // Almacenes
      precintoNumber: 'string', // Número de precinto
      levateDescripcion: 'string' // Descripción del levante
    };
    PdfIndividual.createPDF(inspectionActaDTO, Constants.PDF_MODE_OPEN);

    expect(pdfMakeCreatePdfSpy).toHaveBeenCalledOnceWith(jasmine.any(Object));
    expect(pdfMakeCreatePdfSpy().open).toHaveBeenCalledTimes(1);
    expect(pdfMakeCreatePdfSpy().download).not.toHaveBeenCalled();
  });
});
